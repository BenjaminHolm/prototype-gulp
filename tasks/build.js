var gulp = require('gulp');



// source
gulp.task('sourcebower', function() {
   return gulp.src('./prototype-bower/bower.json')
    .pipe(gulp.dest('./'))
});

gulp.task('sourcecoffee', function() {
   return gulp.src('./prototype-coffee/coffee/**/*')
    .pipe(gulp.dest('./src/coffee'))
});

gulp.task('sourcejade', function () {
  return  gulp.src('./prototype-jade/jade/**/*')
    .pipe(gulp.dest('./src/jade/'))
});

gulp.task('sourcesass', function() {
  return  gulp.src('./prototype-sass/sass/**/*')
    .pipe(gulp.dest('./src/sass'))
});


// wordpress
gulp.task('sourcewpfonts', function() {
  return gulp.src('./build/assets/fonts/*')
    .pipe(gulp.dest('./THEME_NAME/assets/fonts'))
});

gulp.task('sourcewpimg', function() {
  return gulp.src('./build/assets/img/**/*')
    .pipe(gulp.dest('./THEME_NAME/assets/img'))
});


gulp.task('sourcewpjs', function() {
  return gulp.src('./build/assets/js/*.js', '!./build/assets/js/jquery.js')
    .pipe(gulp.dest('./THEME_NAME/assets/js'))
});