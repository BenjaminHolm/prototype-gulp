var gulp = require('gulp');
var imagemin = require('gulp-imagemin');




gulp.task('imageDist', function() {
  gulp.src('./src/img/*')
  .pipe(imagemin({
    optimizationLevel: 5,
    pngquant: true
  }))
  .pipe(gulp.dest('.tmp/img/*'));
});