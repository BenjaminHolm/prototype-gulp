var gulp = require('gulp');
var jade = require('gulp-pug');
var plumber = require('gulp-plumber');



gulp.task('pug', function() {
  return gulp.src('./src/pug/*')
  .pipe(plumber())
  .pipe(jade({
      pretty: true
  }))
  .pipe(gulp.dest('.tmp/html/'));
});