var gulp = require('gulp');
var cmq = require('gulp-combine-media-queries');



gulp.task('cmq', function () {
  gulp.src('./.tmp/css/style.css')
    .pipe(cmq({
      log: true
    }))
  .pipe(gulp.dest('./build/assets/css/'));
});