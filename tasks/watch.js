var gulp = require('gulp');
var watch = require('gulp-watch');



gulp.task('watch', function() {
  gulp.watch('./src/sass/**/*', ['sass']);
  gulp.watch('.tmp/css/*', ['copycss']);
  gulp.watch('./src/pug/**/*', ['pug']);
  gulp.watch('.tmp/html/*', ['copyhtml']);
  gulp.watch('./src/coffee/*', ['coffee']);
  gulp.watch('.tmp/js/*', ['copyjs']);
});


gulp.task('wpwatch', function() {
  gulp.watch('./src/sass/**/*', ['sass']);
  gulp.watch('.tmp/css/*', ['copycss']);
  gulp.watch('./src/pug/**/*', ['pug']);
  gulp.watch('.tmp/html/*', ['copyhtml']);
  gulp.watch('./src/coffee/*', ['coffee']);
  gulp.watch('.tmp/js/**/*.js', ['wpjs']);
  gulp.watch('.tmp/css/style.css', ['wpcss']);
});