var gulp = require('gulp');
var htmlhint = require('gulp-htmlhint');



gulp.task('htmlint', function() {
  gulp.src('build/*.html')
  .pipe(htmlhint())
  .pipe(htmlhint.reporter());
});