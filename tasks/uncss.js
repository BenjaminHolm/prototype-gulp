var gulp = require('gulp');
var uncss = require('gulp-uncss');



gulp.task('uncss', function(){
  gulp.src('.tmp/css/style.css')
  .pipe(uncss({
    html: ['./.tmp/index.html']
  }))
  .pipe(gulp.dest('./build/assets/css'));
});