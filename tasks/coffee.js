var coffee = require('gulp-coffee');
var gulp = require('gulp');
var plumber = require('gulp-plumber');


gulp.task('coffee', function() {
  return gulp.src('./src/coffee/*.coffee')
    .pipe(plumber())
    .pipe(coffee())
    .pipe(gulp.dest('.tmp/js/'));
});