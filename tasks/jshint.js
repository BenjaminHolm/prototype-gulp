var gulp = require('gulp');
var jshint = require('gulp-jshint');



gulp.task('jsLintDev', function() {
  gulp.src('./.tmp/js/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});