var flatten = require('gulp-flatten');
var gulp = require('gulp');



gulp.task('copycss', function() {
  gulp.src('.tmp/css/*')
  .pipe(gulp.dest('./build/assets/css/'))
});


gulp.task('copyfonts', function() {
  gulp.src('./fonts/*')
  .pipe(gulp.dest('./build/assets/fonts'))
});


gulp.task('copyhtml', function() {
  gulp.src(['.tmp/html/*', '!.tmp/html/includes.html'])
  .pipe(gulp.dest('./build/'))
});


gulp.task('copyimg', function() {
  gulp.src('./img/**/*')
  .pipe(gulp.dest('./build/assets/img/'))
});


gulp.task('copyjs', function (){
  gulp.src('.tmp/js/*')
  .pipe(gulp.dest('./build/assets/js/'))
});


gulp.task('copybower', function (){
  gulp.src('./prototype-bower/**/*')
  .pipe(gulp.dest('./'))
});


gulp.task('copycoffee', function (){
  gulp.src('./prototype-coffee/**/*')
  .pipe(gulp.dest('./src/'))
});


gulp.task('copygulp', function (){
  gulp.src('./prototype-gulp/**/*')
  .pipe(gulp.dest('./'))
});


gulp.task('copypug', function (){
  gulp.src('./prototype-pug/**/*')
  .pipe(gulp.dest('./src/'))
});


gulp.task('copysass', function (){
  gulp.src('./prototype-sass/**/*')
  .pipe(gulp.dest('./src/'))
});


// Build bower assets.
gulp.task('buildbowercss', function() {
  gulp.src(['./bower/**/*.css', '!./bower/modernizr/**/*.css'])
  .pipe(flatten())
  .pipe(gulp.dest('./vendor/css/'))
});


gulp.task('buildbowerjs', function() {
  gulp.src(['./bower/**/*.js'])
  .pipe(flatten())
  .pipe(gulp.dest('./vendor/js/'))
});


gulp.task('buildbowersass', function() {
  gulp.src(['./bower/**/*.scss', './bower/**/*.sass'])
  .pipe(flatten())
  .pipe(gulp.dest('./vendor/sass/'))
});


// Build vendor assets.
gulp.task('buildvendorcss', function() {
  gulp.src(['./vendor/**/*.css'])
  .pipe(gulp.dest('./.tmp/'))
});


gulp.task('buildvendorjs', function() {
  gulp.src(['./vendor/**/*.js'])
  .pipe(gulp.dest('./.tmp/'))
});


gulp.task('buildvendorsass', function() {
  gulp.src(['./vendor/**/*.sass'])
  .pipe(gulp.dest('./src/sass/'))
});


// Build wp assets.
gulp.task('wpassets', function() {
  gulp.src(['./.tmp/**/*', '!./.tmp/html/**/*', '!./.tmp/css/style.css'])
  .pipe(gulp.dest('./THEME_NAME/assets/'))
});


gulp.task('wpfonts', function() {
  gulp.src('./fonts/*')
  .pipe(gulp.dest('./THEME_NAME/assets/fonts'))
});


gulp.task('wpimg', function() {
  gulp.src('./img/**/*')
  .pipe(gulp.dest('./THEME_NAME/assets/img'))
});


gulp.task('wpjs', function() {
  gulp.src('./.tmp/js/**/*.js')
  .pipe(gulp.dest('./THEME_NAME/assets/js'))
});