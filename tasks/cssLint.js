var csslint = require('gulp-csslint');
var gulp = require('gulp');
var plumber = require('gulp-plumber');



gulp.task('csslint', function() {
  gulp.src('./build/assets/css/style.css')
  .pipe(csslint())
  .pipe(csslint.reporter());
});