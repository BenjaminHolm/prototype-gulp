var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var replace = require('gulp-replace');


gulp.task('uglifycss', function () {
    return gulp.src('.tmp/css/*.css')
        .pipe(cleanCSS({level:2}))
        .pipe(gulp.dest('./build/assets/css/'))
});


gulp.task('uglifycsswpstyle', function () {
    return gulp.src('.tmp/css/style.css')
        .pipe(replace('../', 'assets/'))
        .pipe(cleanCSS({level:2}))
        .pipe(gulp.dest('./THEME_NAME/'))
});


gulp.task('uglifycsswp', function () {
    return gulp.src(['.tmp/css/*.css', '!.tmp/css/style.css'])
        .pipe(cleanCSS({level:2}))
        .pipe(gulp.dest('./THEME_NAME/assets/css/'))
});