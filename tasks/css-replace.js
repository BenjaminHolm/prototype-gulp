var gulp = require('gulp');
var replace = require('gulp-replace');

gulp.task('wpcss', function(){
    gulp.src(['./.tmp/css/style.css'])
    .pipe(replace('../', 'assets/'))
    .pipe(gulp.dest('./THEME_NAME/'));
});