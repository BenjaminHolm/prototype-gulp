var concat = require('gulp-concat');
var gulp = require('gulp');



gulp.task('concat', function() {
  gulp.src(['./.tmp/assets/css/*.css'])
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./build/assets/css'))
});