var autoprefixer = require('autoprefixer');
var flexibility = require('postcss-flexibility');
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');



gulp.task('sass', function (){
    var processors = [
        autoprefixer,
        flexibility
    ];
    gulp.src('./src/sass/**/*.sass')
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(gulp.dest('.tmp/css'));
});