var gulp = require('gulp');
var uglify = require('gulp-uglify');



gulp.task('uglify', function() {
  gulp.src('./.tmp/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./build/assets/js'))
});