var gulp = require('gulp');
var uglify = require('gulp-uglify');


gulp.task('uglifyjs', function() {
  gulp.src('./.tmp/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./build/assets/js'))
});

gulp.task('uglifyjswp', function() {
  gulp.src('./.tmp/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./THEME_NAME/assets/js'))
});