var gulp = require('gulp');
var plumber = require('gulp-plumber');
var svgmin = require('gulp-svgmin');



gulp.task('svgmin', function() {
  return gulp.src('./.tpm/img/**/*.svg')
  .pipe(plumber())
  .pipe(svgmin([
    {removeDoctype: true},
    {removeComments: true},
    {removeEmptyAttrs: true},
    {removeUselessStrokeAndFill: true}
  ]))
  .pipe(gulp.dest('./build/assets/img/'));
});