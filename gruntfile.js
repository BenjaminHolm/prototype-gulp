module.exports = function(grunt) {

  // measures the time each task takes
  require('time-grunt')(grunt);

  // load grunt config
  require('load-grunt-config')(grunt);

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.registerTask( 'rl', ['watch:uploadLocal'] );
  grunt.registerTask( 'rlive', ['watch:uploadLive'] );
  grunt.registerTask( 'rp', ['watch:uploadProd'] );
};
