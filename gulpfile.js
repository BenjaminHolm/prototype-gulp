var gulp = require('gulp');
var requireDir = require('require-dir');
var seq = require('run-sequence');
var dir = requireDir('./tasks');


// Tasks
gulp.task('default', ['sass', 'pug', 'coffee', 'watch']);
gulp.task('siteassets', ['copyfonts', 'copyimg']);
gulp.task('projectsource', ['buildbower', 'buildcoffee', 'buildpug', 'buildsass']);
gulp.task('bowerassets', ['buildbowerjs', 'buildbowercss', 'buildbowersass', 'buildjs']);
gulp.task('vendorassets', ['buildvendorcss', 'buildvendorjs', 'buildvendorsass']);
gulp.task('projectassets', ['buildvendorcss', 'buildvendorjs', 'buildvendorsass']);
gulp.task('wp', ['sass', 'pug', 'coffee', 'wpcss', 'wpjs', 'wpwatch']);
gulp.task('wpdist', ['sass', 'pug', 'coffee', 'wpcss', 'wpjs', 'wpdistwatch']);
gulp.task('buildwp', ['wpassets', 'wpfonts', 'wpimg']);



// Task sequence
// projectsource
// bowerassets
// vendorassets
// projectassets
// projectclean