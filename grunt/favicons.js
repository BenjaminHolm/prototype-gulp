module.exports = {
  options: {
    trueColor: true,
    precomposed: true,
    windowsTile: true,
    tileBlackWhite: false,
    tileColor: 'auto',
    html: 'build/index.html',
    HTMLPrefix: 'assets/img/favicons/'
  },
  icons: {
    src: 'build/assets/img/favicons/favicon.png',
    dest: 'build/assets/img/favicons/'
  }
};
