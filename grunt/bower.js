module.exports = {
  install: {
    options: {
    targetDir: './bower',
    layout: 'byType',
    install: true,
    verbose: false,
    cleanTargetDir: false,
    cleanBowerDir: true,
    bowerOptions: {}
    }
  }
};