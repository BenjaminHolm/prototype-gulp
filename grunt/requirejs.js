module.exports = {
  dev: {
    options: {
      name: 'main',
      baseUrl: 'dev/js',
      mainConfigFile: 'dev/js/main.js',
      optimize: 'uglify2',
      out: 'dev/js/app.js',
      skipModuleInsertion: false
    }
  }
};
