module.exports = {
  upLive: {
    options: {spawn: false},
    files: ['*'],
    tasks: ['rsync:live']
  },
  upLocal: {
    options: {spawn: false},
    files: ['THEME_NAME/*'],
    tasks: ['rsync:local']
  }
};