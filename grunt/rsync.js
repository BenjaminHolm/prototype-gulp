module.exports = {
  options: {
  args: ["--verbose"],
  exclude: [".git*","*.scss", "*.sass", "node_modules"],
  recursive: true,
  ssh: true
  },
  live: {
    options: {
      src: "THEME_NAME",
      dest: "~/public_html/wp-content/themes/",
      host: "",
      port: "2222",
      syncDestIgnoreExcl: true
    }
  },
  local: {
    options: {
      src: "THEME_NAME",
      dest: "/srv/www/XXXXXXXX/htdocs/wp-content/themes/",
      host: "vagrant@vvv",
      syncDestIgnoreExcl: true,
    }
  },
  prod: {
    options: {
      src: "THEME_NAME",
      dest: "~/public_html/prod.sitename.com/wp-content/themes/",
      host: "",
      port: "2222",
      syncDestIgnoreExcl: true
    }
  }
};
