module.exports = {
  options: {
  args: ["--verbose"],
  exclude: [".git*","*.scss", "*.sass", "node_modules"],
  recursive: true,
  ssh: true
  },
  dev: {
    options: {
      src: "THEME_NAME",
      dest: "~/public_html/XXXXXXXX/wp-content/themes/",
      host: "",
      port: "2222",
      syncDestIgnoreExcl: true
    }
  },
  live: {
    options: {
      src: "THEME_NAME",
      dest: "~/public_html/wp-content/themes/",
      host: "",
      port: "2222",
      syncDestIgnoreExcl: true
    }
  },
  local: {
    options: {
      src: "THEME_NAME",
      dest: "~/Local/THEME_NAME/app/public/wp-content/themes/",
      syncDestIgnoreExcl: true,
    }
  }
};
